import Vue from 'vue';
import VueRouter from 'vue-router';
import addUser from '../views/AddUser.vue';
import login from '../views/login.vue';
import UserInfo from '../views/UserInfo.vue'; // 添加了对应的UserInfo.vue组件
Vue.use(VueRouter);

const routes = [
  {
    path: '/adduser',
    name: 'addUser',
    // route level code-splitting
    // this generates a separate chunk (addUser.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "addUser" */ '../views/AddUser.vue');
    }
  },
  {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (addUser.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function () {
        return import(/* webpackChunkName: "login" */ '../views/login.vue');
      }
    },
  {
    path: '/userinfo',
    name: 'userinfo',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "userinfo" */ '../views/UserInfo.vue');
        }
    },
  {
      path: '/todolist',
      name: 'todolist',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function () {
        return import(/* webpackChunkName: "todolist" */ '../views/TodoList.vue');
      }
    },
        {
          path: '/addtodo',
          name: 'addtodo',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "addtodo" */ '../views/AddTask.vue')
        },
        {
          path: '/edit-task',
          name: 'edit-task',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "edit-tack" */ '../views/EditTask.vue')
        }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
