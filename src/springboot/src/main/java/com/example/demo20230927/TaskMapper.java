package com.example.demo20230927;


import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TaskMapper {

    List<Task> findByUserId(Integer userId);

    void insertTask(Task task);

    Task findTaskById(Long taskId);
    void updateTask(Task task); // 添加一个更新任务的方法
}
