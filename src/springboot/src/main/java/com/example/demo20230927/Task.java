package com.example.demo20230927;

import lombok.Data;

@Data
public class Task {

    private Integer id;
    private String name;
    private Integer uid; // 用户ID
    private boolean completed;

    // 省略构造函数、getter和setter方法

}


