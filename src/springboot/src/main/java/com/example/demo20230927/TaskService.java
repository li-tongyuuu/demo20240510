package com.example.demo20230927;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskMapper taskMapper;

    @Transactional(readOnly = true)
    public List<Task> getTasksByUserId(Integer userId) {
        return taskMapper.findByUserId(userId);
    }

    public void addTask(Task task) {
        taskMapper.insertTask(task);
    }

    public Task findTaskById(Long taskId) {
        return taskMapper.findTaskById(taskId);
    }
    @Transactional
    public void updateTask(Task updatedTask) {
        // 获取原始任务信息
        Task existingTask = taskMapper.findTaskById(Long.valueOf(updatedTask.getId()));
        if (existingTask == null) {
            throw new RuntimeException("任务不存在");
        }

        // 更新任务信息
        existingTask.setName(updatedTask.getName());
        existingTask.setCompleted(updatedTask.isCompleted());

        // 保存更新后的任务信息到数据库
        taskMapper.updateTask(existingTask);
    }
}
