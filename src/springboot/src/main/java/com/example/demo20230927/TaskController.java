package com.example.demo20230927;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;  // 确保 UserService 能够被注入

    @GetMapping("/mytasks")
    public ResponseEntity<?> getMyTasks(@RequestHeader("Authorization") String token) {
        try {
            // 从 token 中获取用户信息
            User user = userService.getUserInfoByToken(token);
            // 使用用户ID获取任务列表
            List<Task> tasks = taskService.getTasksByUserId(user.getId());
            return ResponseEntity.ok(tasks);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }

    @PostMapping("/add")
    public ResponseEntity<?> addTask(@RequestBody Task task, @RequestHeader("Authorization") String token) {
        try {
            // 可能需要从 token 解析用户 ID
            User user = userService.getUserInfoByToken(token);
            task.setUid(user.getId());  // 设置任务的用户 ID

            taskService.addTask(task);
            return ResponseEntity.ok("任务添加成功");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("任务添加失败: " + e.getMessage());
        }
    }

    // 在 TaskController.java 中
    @GetMapping("/{taskId}")
    public ResponseEntity<?> getTaskById(@PathVariable Long taskId, @RequestHeader("Authorization") String token) {
        try {
            System.out.println(token);
            User user = userService.getUserInfoByToken(token);
            Task task = taskService.findTaskById(taskId);

            if (task != null && task.getUid().equals(user.getId())) {
                return ResponseEntity.ok(task);
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("访问被拒绝");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("服务器错误: " + e.getMessage());
        }
    }
    @PostMapping("/edit-task/{taskId}")
    public ResponseEntity<?> updateTask(@PathVariable Long taskId, @RequestBody Task updatedTask, @RequestHeader("Authorization") String token) {
        try {
            // 获取当前登录用户信息
            User user = userService.getUserInfoByToken(token);

            // 根据任务ID从数据库中获取任务信息
            Task existingTask = taskService.findTaskById(taskId);

            // 检查任务是否存在
            if (existingTask == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("任务不存在");
            }

            // 检查当前用户是否有权限修改该任务
            if (!existingTask.getUid().equals(user.getId())) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("无权修改此任务");
            }
            // 更新任务信息
            existingTask.setName(updatedTask.getName());
            existingTask.setCompleted(updatedTask.isCompleted());

            // 保存更新后的任务信息到数据库
            taskService.updateTask(existingTask);
            return ResponseEntity.ok("任务修改成功");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("服务器错误: " + e.getMessage());
        }
    }

}