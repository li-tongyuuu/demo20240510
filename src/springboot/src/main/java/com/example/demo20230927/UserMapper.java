package com.example.demo20230927;


import org.apache.ibatis.annotations.Mapper;


//假设已有import语句
@Mapper
public interface UserMapper {
    void insertUser(User user);

    User findUserByUsername(String username);
}
