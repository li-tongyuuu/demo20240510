CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `user` (`name`, `age`) VALUES ('Tom', 20);
INSERT INTO `user` (`name`, `age`) VALUES ('Jerry', 25);
